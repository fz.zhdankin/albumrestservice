package rest.responses;

import java.util.List;

/** Альбом, облегчённая версия(без изображений), ответ для запросов: /albums, /albums/create **/
public class AlbumResponseLite {
	/** Идентификатор альбома. **/
	private long id;
	/** Название альбома. **/
	private String name;

	// todo: Zhdankin: зачем в объете поле ошибки?
	private String error;


	public AlbumResponseLite(long id, String name) {
		this.id = id;
		this.name = name;
	}
	

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
