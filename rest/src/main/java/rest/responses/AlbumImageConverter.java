package rest.responses;

import java.util.ArrayList;
import java.util.List;

import rest.entity.AlbumImage;

public class AlbumImageConverter {
	
	/** Конвертирует AlbumImage в AlbumImageResponse **/
	public AlbumImageResponse Convert(AlbumImage albumImage) {
		return new  AlbumImageResponse(albumImage.getId(), albumImage.getFileName());
	}
	
	/** Конвертирует список AlbumImage в список AlbumImageResponse **/
	public List<AlbumImageResponse> Convert(List<AlbumImage> albumImages){
		ArrayList<AlbumImageResponse> retValue = new ArrayList<>(albumImages.size());
		for(AlbumImage item : albumImages)
			retValue.add(Convert(item));
		return retValue;
	}
}
