package rest.responses;

/**
 * Информация о изображении из альбома. Легковесный вариант, содержит только
 * название и идентификтаор изображения.
 * 
 * @see AlbumResponse Составная часть ответа на запрос /album{id}.
 **/
public class AlbumImageResponse {

	/** Идентификатор изображения **/
	private long id;
	/** Название изображения **/
	private String name;

	
	public AlbumImageResponse(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
