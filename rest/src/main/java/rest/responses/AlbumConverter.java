package rest.responses;

import java.util.ArrayList;
import java.util.List;

import rest.entity.Album;
import rest.entity.AlbumImage;

public class AlbumConverter {

	private AlbumImageConverter imageConverter = new AlbumImageConverter();
	
	/** Конвертирует Album в AlbumResponseLite. **/
	public AlbumResponseLite Convert(Album album) {
		return new AlbumResponseLite(album.getId(), album.getAlbumName());
	}

	/** Конвертирует список Album в список AlbumResponseLite **/
	public List<AlbumResponseLite> Convert(List<Album> albums) {
		ArrayList<AlbumResponseLite> retValue = new ArrayList<>(albums.size());
		for (Album item : albums)
			retValue.add(Convert(item));
		return retValue;
	}

	public AlbumResponse ConvertToAlbumResponse(Album album) {
		return new AlbumResponse(album.getId(), album.getAlbumName(), imageConverter.Convert(album.getImages()));
	}
}

