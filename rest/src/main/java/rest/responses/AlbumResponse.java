package rest.responses;

import java.util.List;

/**
 * Альбом содержащий облегчённый список изображений(только имена изображений и
 * идентификаторы), ответ для запроса /album{id}.
 **/
public class AlbumResponse {

	/** Идентификатор изображения **/
	private long id;
	/** Название изображения **/
	private String name;
	/** список изображений(облегчённый), хранящихся в альбоме. **/
	private List<AlbumImageResponse> images;
	/** Ошибка. **/
	private String error;

	public AlbumResponse(long id, String name, List<AlbumImageResponse> images) {
		super();
		this.id = id;
		this.name = name;
		this.images = images;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AlbumImageResponse> getImages() {
		return images;
	}

	public void setImages(List<AlbumImageResponse> images) {
		this.images = images;
	}

}
