package rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

import rest.entity.Album;
import rest.entity.AlbumImage;
import rest.entity.AlbumRepository;
import rest.entity.ImageRepository;
import rest.responses.AddImageResponse;
import rest.responses.AlbumConverter;
import rest.responses.AlbumImageConverter;
import rest.responses.AlbumImageResponse;
import rest.responses.AlbumResponse;
import rest.responses.AlbumResponseLite;

@RestController
@RequestMapping("/api")
public class AlbumController {

	/* todo: Zhdankin: лучше сделать инжект через конструктор */
	@Autowired
	private AlbumRepository albumRepo;
	@Autowired
	private ImageRepository imageRepo;

	/* todo: Zhdankin: конвертеры тоже нужно инжектить */
	private AlbumConverter albumConverter = new AlbumConverter();
	private AlbumImageConverter imageConverter = new AlbumImageConverter();

	@GetMapping("/albums")
	// todo: Zhdankin: по код-стайлу джавы, следует писать методы со строчной буквы, но в рамках ТЗ не критично
	public ResponseEntity<List<AlbumResponseLite>> GetAlbums() {
		/* todo: Zhdankin: нужен слой сервисов между контролером и репозиторием - туда вынести вызов конвертера. Так для других методов контроллера */
		List<AlbumResponseLite> retValue = albumConverter.Convert(Lists.newArrayList(albumRepo.findAll()));
		return new ResponseEntity<>(retValue, HttpStatus.OK);
	}

	@PostMapping("/albums/create")
	/* todo: Zhdankin: сделать передачу имени альбома не параметром, а в теле json */
	public ResponseEntity<AlbumResponseLite> CreateAlbum(@RequestParam(value = "albumName") String albumName) {
		Album createdAlbum = albumRepo.save(new Album(-1, albumName, new ArrayList<AlbumImage>()));
		if (createdAlbum.getId() == -1) {
			return new ResponseEntity<>(new AlbumResponseLite(-1, albumName), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(albumConverter.Convert(createdAlbum), HttpStatus.OK);
	}

	@DeleteMapping("/albums/delete/{id}")
	public ResponseEntity<List<AlbumResponseLite>> DeleteAlbum(@PathVariable long id) {
		Optional<Album> findedAlbum = albumRepo.findById(id);
		if (!findedAlbum.isPresent()) {
			// todo: Zhdankin: нужно более корректное решение для возврата разного типа объектов
			return new ResponseEntity("Альбом с id: " + id + "не найден", HttpStatus.NOT_FOUND);
		}
		albumRepo.deleteById(id);
		/* todo: Zhdankin: зачем нужно чтобы delete-метод возвращал список альбомов? */
		return new ResponseEntity<List<AlbumResponseLite>>(albumConverter.Convert(Lists.newArrayList(albumRepo.findAll())), HttpStatus.OK);
	}

	@PostMapping("album/{id}/addImage")
	public ResponseEntity<AddImageResponse> AddImage(@RequestParam MultipartFile file, @PathVariable long id)
			throws IOException {

		if (file.isEmpty()) {
			return new ResponseEntity<>(new AddImageResponse(0, " ", 0, "Пустой файл", ""), HttpStatus.LENGTH_REQUIRED);
		}

		if (!file.getContentType().contains("image")) {
			return new ResponseEntity<AddImageResponse>(
					new AddImageResponse(-1, file.getName(), file.getSize(), "вы загрузили файл с расширением:"
							+ file.getContentType() + " расширение должно быть: JPEG,PNG,GIF", ""),
					HttpStatus.BAD_REQUEST);
		}

		Optional<Album> findedAlbum = albumRepo.findById(id);
		if (!findedAlbum.isPresent()) {
			return new ResponseEntity<AddImageResponse>(new AddImageResponse(-1, file.getName(), file.getSize(),
					"альбом с идентификатором: " + id + " не найден", ""), HttpStatus.BAD_REQUEST);
		}
		Album album = findedAlbum.get();
		album.AddImage(new AlbumImage((long) 0, file.getName(), file.getBytes(), album, file.getContentType()));
		albumRepo.save(album);
		return new ResponseEntity<AddImageResponse>(
				new AddImageResponse(0, file.getName(), file.getSize(), "", file.getContentType()), HttpStatus.OK);
	}

	@GetMapping("album/{id}")
	public ResponseEntity<AlbumResponse> GetAlbum(@PathVariable long id) {
		Optional<Album> findedAlbum = albumRepo.findById(id);

		if (!findedAlbum.isPresent()) {
			return new ResponseEntity<AlbumResponse>(new AlbumResponse(-1, "", null), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<AlbumResponse>(albumConverter.ConvertToAlbumResponse(findedAlbum.get()),
				HttpStatus.OK);
	}

	@GetMapping("album/{albumId}/download/{imageId}")
	// todo: Zhdankin: зачем нужна переменная albumId?
	public HttpEntity<byte[]> DownloadImage(@PathVariable long albumId, @PathVariable long imageId) {

		Optional<AlbumImage> findedImage = imageRepo.findById(imageId);
		if (!findedImage.isPresent()) {
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "image id:" + imageId + "notfound");
			return new HttpEntity<byte[]>(null, headers);
		}

		byte[] image = findedImage.get().getContent();
		HttpHeaders headers = new HttpHeaders();

		switch (findedImage.get().getFormat()) {
		case "image/jpeg":
			headers.setContentType(MediaType.IMAGE_JPEG);
			break;
		case "image/png":
			headers.setContentType(MediaType.IMAGE_PNG);
			break;
		case "image/gif":
			headers.setContentType(MediaType.IMAGE_GIF);
			break;
		default:
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			break;
		}

		headers.setContentLength(image.length);
		/* todo: Zhdankin: нужно как то возвращать оригинальное файл, то, что было, когда файл загрузили */
		return new HttpEntity<byte[]>(image, headers);
	}

	@DeleteMapping("album/{albumId}/deleteImage/{imageId}")
	public ResponseEntity<AlbumResponse> DeleteImage(@PathVariable long albumId, @PathVariable long imageId) {
		Optional<Album> findedAlbum = albumRepo.findById(albumId);

		if (!findedAlbum.isPresent()) {
			return new ResponseEntity("Альбом с id: " + albumId + "не найден", HttpStatus.NOT_FOUND);
		}

		Optional<AlbumImage> findedImage = imageRepo.findById(imageId);
		if (!findedImage.isPresent()) {
			return new ResponseEntity("Изображение с id: " + albumId + "не найден", HttpStatus.NOT_FOUND);
		}
		
		Album album = findedAlbum.get();
		if(!album.IsContainsImage(findedImage.get())) {
			return new ResponseEntity("Изображение с id:" + imageId + " не содержится в альбоме с id:" + albumId, HttpStatus.NOT_FOUND);
		}
		album.DeleteImageById(imageId);
		imageRepo.delete(findedImage.get());

		return new ResponseEntity<AlbumResponse>(
				albumConverter.ConvertToAlbumResponse(albumRepo.findById(albumId).get()), HttpStatus.OK);
	}

	@PutMapping("album/{id}/rename")
	public ResponseEntity<AlbumResponse> RenameAlbum(@RequestParam(value = "newName") String newName,
			@PathVariable long id) {
		Optional<Album> finded = albumRepo.findById(id);
		if (!finded.isPresent()) {
			return new ResponseEntity("Альбом с id:" + id + " не найден", HttpStatus.NOT_FOUND);
		}
		Album updating = finded.get();
		updating.setAlbumName(newName);
		albumRepo.save(updating);

		Album updated = albumRepo.findById(id).get();
		return new ResponseEntity<AlbumResponse>(albumConverter.ConvertToAlbumResponse(updated), HttpStatus.OK);
	}
}
