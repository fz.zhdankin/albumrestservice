package rest.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
 * Класс описывающий изображение, содержащиеся в альбоме. Хранимая сущность.
 * 
 * @see Album
 **/
@Entity
public class AlbumImage {
	/** Идентификатор изображения, содержащегося в альбоме. **/
	@Id
	@GeneratedValue
	// todo: Zhdankin: по такому id сейчас можно предсказать кол-во изображений и скачать любое изображение, давай попробуем както использовать UUID?
	private Long id;
	/** Имя файла, содержащего изображение. **/
	private String fileName;
	/** Содержимое, т.е. само изображение. **/
	@Lob
	private byte[] content;
	@ManyToOne
	private Album album;
	/** формат изображения png/jpg/gif **/
	private String format;

	public AlbumImage(Long id, String fileName, byte[] content, Album album, String format) {
		this.id = id;
		this.fileName = fileName;
		this.content = content;
		this.album = album;
		this.format = format;
	}

	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	protected AlbumImage() {
	}
}
