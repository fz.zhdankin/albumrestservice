package rest.responses;

/** Ответ на запрос album/{id}/addImage **/
public class AddImageResponse {
	/** Идентификатор добавленного в альбом изображения **/
	private long id;
	/** название добавленного в альом изображения **/
	private String name;
	/** размер добавленного в альбом изображения **/
	private long size;
	/** текст ошибки, если таковая имеется **/
	private String error;
	/** формат **/
	private String format;

	public AddImageResponse(long id, String name, long size, String error, String format) {
		this.id = id;
		this.name = name;
		this.size = size;
		this.error = error;
		this.format = format;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

}
