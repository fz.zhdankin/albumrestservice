package rest.entity;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.List;

/** Альбом с изображениями. Хранимая сущность. **/
@Entity
public class Album {
	/** Идентификатор альбома **/
	@Id
	@GeneratedValue
	private Long id;
	/** Название альбома **/
	private String albumName;
	/** Изображения, содержащиеся в альбоме. **/

	/* todo: Zhdankin: точно ли нужно использование ElementCollection для Entity? */
	@ElementCollection
	@OneToMany(cascade = CascadeType.ALL,  orphanRemoval = true)//(cascade = CascadeType.ALL)
	private List<AlbumImage> images;

	protected Album() {}
	
	public Album(long id, String albumName, List<AlbumImage> images) {
		this.id = id;
		this.albumName = albumName;
		this.images = images;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public List<AlbumImage> getImages() {
		return images;
	}

	protected void setImages(List<AlbumImage> images) {
		this.images = images;
	}
	
	public void AddImage(AlbumImage image) {
		/* todo: Zhdankin: эту логику лучше вынести в сервисный слой */
		this.images.add(image);
	}
	
	public void DeleteImageById(long id) {
		/* todo: Zhdankin: эту логику лучше вынести в сервисный слой */
		AlbumImage image = null;
		for(AlbumImage img : images ) {
			if(img.getId() == id) {
				image = img;
			}
		}
		if(image != null) {
			images.remove(image);
		}
	}
	
	public boolean IsContainsImage(AlbumImage image) {
		/* todo: Zhdankin: эту логику лучше вынести в сервисный слой */
		return images.contains(image);
		
	}

}
