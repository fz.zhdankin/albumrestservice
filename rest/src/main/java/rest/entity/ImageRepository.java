package rest.entity;

import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<AlbumImage, Long>{

}
